<?php
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/src/DBConnector.php';
require_once __DIR__ . '/src/DataValidator.php';
require_once __DIR__ . '/src/Controller.php';

(new Controller())->main();
?>

<!doctype html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Formularz</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
            crossorigin="anonymous"></script>

</head>
<body>
<section class="jumbotron">
    <form method="post" action="">
        <?php
        if (empty($_SESSION['manager_name'])) {
            echo 'Podaj swoje imię <input name="manager_name">';
        } else {
            echo '
            <div>Cześć ' . $_SESSION['manager_name'] . '</div>
            <input type="text" name="company_name"> Nazwa firmy/ośrodka<br>
            <input type="text" name="link"> Link<br>
            <input type="text" name="phone"> Numer telefonu<br>
            <input type="email" name="email"> E-mail<br>
            <input type="text" name="contact_name"> Osoba kontaktowa<br>
            <input type="text" name="location"> Lokalizacja<br>
            <select name="discipline">
                <option>Piłka nożna</option>
                <option>Siatkówka</option>
                <option>Piłka ręczna</option>
                <option>Koszykówka</option>
                <option>Hokej</option>
                <option>Sztuki walki</option>
                <option>Pływanie</option>
                <option>Treningi indywidualne</option>
                <option>Inne</option>
            </select> Dyscyplina<br><br>
            ';
        } ?>
        <input type="submit" class="btn btn-success" value="Wyślij">
    </form>
</section>
</body>
</html>
