<?php

class Controller
{
    private DataValidator $dataValidator;

    private DBConnector $dbConnector;

    public function __construct()
    {
        $this->dataValidator = new DataValidator(); //DI is harder than I thought xD
        $this->dbConnector = DBConnector::getInstance();
    }

    public function main(): void
    {
        session_start();

        if (empty($_POST)) {
            return;
        }

        if (!empty($_POST['manager_name'])) {
            $this->saveManagerName();
            return;
        }

        if (!$this->dataValidator->validate($_POST)) {
            echo 'Wprowadzone dane są nieprawidłowe lub już znajdują się w arkuszu. Błąd walidacji.';
            return;
        }
        $data = [
            'company_name' => $_POST['company_name'],
            'link' => $_POST['link'],
            'phone' => $_POST['phone'],
            'email' => $_POST['email'],
            'contact_name' => $_POST['contact_name'],
            'discipline' => $_POST['discipline'],
            'location' => $_POST['location'],
            'who_added' => $_SESSION['manager_name']
        ];

        $this->dbConnector->insert($data);

        echo '<h2>Dodano rekord</h2>';
    }

    private function saveManagerName(): void
    {
        if (empty($_SESSION['manager_name']) && !empty($_POST['manager_name'])) {
            $_SESSION['manager_name'] = $_POST['manager_name'];
        }
    }
}