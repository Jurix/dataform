<?php

class DataValidator
{
    private DBConnector $dbConnector;

    public function __construct()
    {
        $this->dbConnector = DBConnector::getInstance();
    }

    public function validate(array $data): bool
    {
        $result = $this->dbConnector->get();
        if (empty($result)) {
            return true;
        }

        if (count(array_filter($data)) < 4) {
            return false;
        }

        if (!empty($data['company_name']) && in_array($data['company_name'], array_column($result, '0'))) {
            return false;
        }

        if (!empty($data['link']) && in_array($data['link'], array_column($result, '1'))) {
            return false;
        }

        if (!empty($data['phone']) && in_array($data['phone'], array_column($result, '2'))) {
            return false;
        }

        if (!empty($data['email']) && in_array($data['email'], array_column($result, '3'))) {
            return false;
        }

        return true;
    }
}