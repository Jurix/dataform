<?php

class DBConnector
{
    private const SHEET_ID = '1U4OFeAdSOouZF9bEkeXs3edebDU4suBOKgQo5pbN5T8';

    private const TABLE_NAME = 'dane';

    private Google_Service_Sheets $service;

    private static $instances = [];

    protected function __construct() { }

    protected function __clone() { }

    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }

    public static function getInstance(): DBConnector
    {
        $dbConnector = static::class;
        if (!isset(self::$instances[$dbConnector])) {
            self::$instances[$dbConnector] = new static();
        }
        self::$instances[$dbConnector]->createConnection();

        return self::$instances[$dbConnector];
    }

    public function createConnection(): Google_Service_Sheets
    {
        $client = new Google_Client();
        $client->setApplicationName('Dane firmy sportowe');
        $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
        $client->setAccessType('offline');
        $client->setAuthConfig(__DIR__ . '/../credentials.json');

        $this->service = new Google_Service_Sheets($client);

        return $this->service;
    }

    public function insert(array $data): void
    {
        $body = new \Google_Service_Sheets_ValueRange([
            'values' => [
                array_values($data)
            ]
        ]);
        $response = $this->service->spreadsheets_values->append(
            self::SHEET_ID,
            self::TABLE_NAME,
            $body,
            ['insertDataOption' => 'INSERT_ROWS', 'valueInputOption' => 'RAW']
        );
    }

    public function get(): array
    {
        return $this->service->spreadsheets_values->get(
            self::SHEET_ID,
            self::TABLE_NAME . '!A2:F9999'
            )->getValues() ?? [];
    }
}